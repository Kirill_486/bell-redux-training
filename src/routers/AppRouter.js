import React from 'react';
import { BrowserRouter, Route, Switch, Link, NavLink } from 'react-router-dom';
import NotFoundPage from '../components/NotFoundPage';
import Header from '../components/Header';

import CreateEventPage from '../components/CreateEventPage';
import EditEventPage from '../components/EditEventPage';
import DashboardPage from '../components/DashboardPage';
import ContactPage from '../components/ContactPage';

// import HomePage from '../components/HomePage';
// import PortfolioItemPage from '../components/PortfolioItemPage';
// import PortfolioPage from '../components/PortfolioPage';

const AppRouter = () => (
  <BrowserRouter>
    <div>
      <Header />
      <Switch>
        <Route path="/" component={DashboardPage} exact={true} />
        <Route path="/create" component={CreateEventPage} exact={true} />
        <Route path="/edit/:id" component={EditEventPage} />
        <Route path="/contact" component={ContactPage} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>
  </BrowserRouter>
);

export default AppRouter;
