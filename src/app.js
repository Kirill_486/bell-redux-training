import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import AppRouter from './routers/AppRouter';
import store from './store/createStore';

import 'normalize.css/normalize.css';
import './styles/styles.scss';

const appJsx = (
  <Provider store={store}>
    <AppRouter />    
  </Provider>
);

ReactDOM.render(appJsx, document.getElementById('app'));
