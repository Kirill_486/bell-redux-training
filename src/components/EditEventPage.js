import React from 'react';
import { connect } from 'react-redux';

import EventForm from './EventForm';
import { editEvent } from '../store/actions/eventsActions';

class EditEventPage extends React.Component {
    
    onFormSubmit = (event) => {
        this.props.dispatchEditEvent(event);
        this.props.history.push('/');
    }

    render() {
        return(
            <div>
                <p>This is the Edit Event Page</p>
                <p>{`Lets edit event id: ${this.props.match.params.id}`}</p>
                <EventForm
                    id={this.props.event.id}
                    title={this.props.event.title}
                    description={this.props.event.description}
                    startsAt={this.props.event.startsAt}
                    onFormSubmit={this.onFormSubmit} 
                />
            </div>
        );
    }
}

const mapStateToProps = (state, props) => ({
    event: state.events.find((event) => event.id == props.match.params.id)
});

const mapDispatchToProps = (dispatch) => ({
    dispatchEditEvent: (event) => dispatch(editEvent(event))
  });

export default connect(mapStateToProps, mapDispatchToProps)(EditEventPage);