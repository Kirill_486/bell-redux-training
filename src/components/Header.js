import React from 'react';
import { NavLink } from 'react-router-dom';
import moment from 'moment';

const createRouteRegexp = /create/;
const editRouteRegexp = /edit/;
const contactsRouteRegexp = /contact/;

// const Header = () => (
//   <header>
//     <h1>Event calendar</h1>
//     <div>
//       Today is the {moment().format()}
//       <div>
//         <NavLink to="/create">Create na event</NavLink>
//       </div>
//     </div>    
//   </header>
// );

// {window.location.pathname === '/create'}
// {window.location.pathname === '/create'}

const Header = (props) => {
  return (
  <header>
    <h1>Event calendar</h1>
    <div>
      Today is the {moment().format()}
      <div>
        { window.location.pathname !== '/' && <div><NavLink to="/">Events Dashboard</NavLink></div>  }
        { window.location.pathname !== '/create' && <div><NavLink to="/create">Create an event</NavLink></div> }
        { window.location.pathname !== '/contact' && <div><NavLink to="/contact">Contact info</NavLink></div> }        
      </div>
    </div>    
  </header>
);
};

export default Header;
