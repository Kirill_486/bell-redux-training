import React from 'react';
import { connect } from 'react-redux';

import EventForm from './EventForm';
import { addEvent } from '../store/actions/eventsActions';

class CreateEventPage extends React.Component {
  onFormSubmit = (event) => {
    this.props.dispatchAddEvent(event);
    this.props.history.push('/');
  };

  render() {
    return (
      <div>
        <p>This is the create Event Page</p>
        <EventForm onFormSubmit={this.onFormSubmit} />
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => ({
  dispatchAddEvent: (event) => dispatch(addEvent(event))
});

export default connect(undefined, mapDispatchToProps)(CreateEventPage);

// const mapDispatchToProps = (dispatch) => ({
//   startAddExpense: (expense) => dispatch(startAddExpense(expense))
// });


// export default CreateEventPage;
