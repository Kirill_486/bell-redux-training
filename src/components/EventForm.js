import React from 'react';
import moment from 'moment';
import DateTimePicker from 'material-ui-pickers/DateTimePicker';
import MuiPickersUtilsProvider from 'material-ui-pickers/utils/MuiPickersUtilsProvider';
import MomentUtils from 'material-ui-pickers/utils/moment-utils';

class EventForm extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            id: props.id || "",
            title: props.title || "",
            description: props.description || "",
            startsAt: props.startsAt || moment().valueOf()
        }

        this.onSubmit = this.onSubmit.bind(this);
        this.onTitleChange = this.onTitleChange.bind(this);
        this.onDescriptionChange = this.onDescriptionChange.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
    }

    onSubmit(e) {
        e.preventDefault();
        //no validation yet
        this.props.onFormSubmit({
            id: this.state.id,
            title: this.state.title,
            description: this.state.description,
            startsAt: this.state.startsAt
        });        
    }

    onTitleChange(e) {
        const title = e.target.value
        this.setState({
            title
        });
    }

    onDescriptionChange(e) {
        const description = e.target.value;
        this.setState({
            description
        });
    }

    handleDateChange(date) {        
        const startsAt = date.valueOf();
        this.setState({
            startsAt
        });
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit}>
                    <div>
                        <label htmlFor="title">Нвание</label>
                        <input 
                            type="text" 
                            name="title" 
                            value={this.state.title}
                            onChange={this.onTitleChange}
                        />
                    </div>
                    <div>
                        <label htmlFor="description">Описание</label>
                        <textarea 
                            type="text" 
                            name="description" 
                            value={this.state.description}
                            onChange={this.onDescriptionChange}
                        />
                    </div>
                    <div>
                        <MuiPickersUtilsProvider utils={MomentUtils}>
                            <span>Starts at</span>
                            <DateTimePicker
                                value={this.state.startsAt}
                                onChange={this.handleDateChange}
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                    <div>
                        <input type="submit" value="Сохранить" />                        
                    </div>
                </form>
            </div>
        );
    }
}

export default EventForm;