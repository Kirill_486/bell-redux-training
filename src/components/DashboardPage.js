import React from 'react';
import { connect } from 'react-redux';

import EventsDashboardItem from './EventsDashboardItem';

const filterEvents = (events, timestamp) => {
  return events
    .filter((event)=> {
      return event.startsAt > timestamp;
    })
    .sort((a, b)=> a.startsAt > b.startsAt);
}

const DashboardPage = (props) => (
  <div>
    <h1>Upcoming events:</h1>
    {props.events.map(
      (event) => 
        <EventsDashboardItem
          key={event.id} 
          id={event.id} 
          title={event.title} 
          description={event.description} 
          startsAt={event.startsAt} 
        /> 
      )
    }
  </div>
);

const mapStateToProps = (state) => {
  return {
    events: filterEvents(state.events, state.filterDate)
    // events: state.events
  }
};

export default connect(mapStateToProps)(DashboardPage);