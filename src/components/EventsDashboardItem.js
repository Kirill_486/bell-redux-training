import React from 'react';
import moment from 'moment';
import {Link} from 'react-router-dom';

const EventsDashboardItem = (props) => (
    <div>
        <div>{props.title}</div>
        <div>{props.description}</div>
        <div>{moment(props.startsAt).format()}</div>
        <div><Link to={`/edit/${props.id}`}>Edit</Link></div>
    </div>
);

export default EventsDashboardItem;