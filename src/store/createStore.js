import {createStore, combineReducers} from 'redux';

import eventsReducer from './reducers/eventsReducer';
import dateReducer from './reducers/dateReducer';

// import {addEvent, editEvent, deleteEvent} from './actions/eventsActions';
// import {setDate, resetDate} from './actions/dateActions';

const store = createStore(combineReducers({
    events: eventsReducer,
    filterDate: dateReducer
    })
);

// const unsubscribe = store.subscribe(() => {
//     console.log(store.getState());
//   });

export default store;