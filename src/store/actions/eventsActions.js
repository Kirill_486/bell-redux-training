import moment from 'moment';

export const addEvent = ({
        title = 'no_title', 
        description = 'no description',
        startsAt = moment().valueOf()
    } = {}) => ({
    type: 'ADD_EVENT',
    payload: {
        title,
        description,
        startsAt
    }
});

export const editEvent = ({
    id,
    title = 'no_title', 
    description = 'no description',
    startsAt = moment().valueOf()
    } = {}) => ({
    type: 'EDIT_EVENT',
    payload: {
        id,
        title,
        description,
        startsAt
    }
});

export const deleteEvent = (id) => ({
    type: 'DELETE_EVENT',
    payload: id
});