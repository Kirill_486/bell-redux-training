import moment from 'moment';

// console.log(moment());
// console.log(moment().valueOf());

export const setDate = (timeStamp = moment().valueOf()) => ({
    type: 'SET_DATE',
    payload: timeStamp
});

export const resetDate = () => ({
    type: 'RESET_DATE'
});