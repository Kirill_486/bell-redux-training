import uuid from 'uuid';

const demoStoreEvents = [
    {
        id: 1,
        title: 'First Event',
        description: 'First Event Description',
        startsAt: 1531765000000
    },
    {
        id: 2,
        title: 'Second Event',
        description: 'Second Event Description',
        startsAt: 1531670000000
    }
];

const eventsReducer = (state = demoStoreEvents, action) => {
    switch (action.type) {
        case 'ADD_EVENT':
            action.payload.id = uuid();
            // console.log('ADD_EVENT', action.payload);
            return [...state, action.payload];
        case 'EDIT_EVENT' : 
            // console.log('EDIT_EVENT', action.payload);
            return state.map(
                (event)=> event.id == action.payload.id ? action.payload : event
            );
        case 'DELETE_EVENT' :
            // console.log('DELETE_EVENT', action.payload.id);
            return state.filter(
                (event)=>event.id != action.payload.id
            );
        default: return state;
    }
};

export default eventsReducer;