import moment from 'moment';

// console.log(moment());
// console.log(moment().valueOf());

const dateReducer = (state = moment().valueOf(), action) => {
    switch (action.type) {
        case 'SET_DATE':
            return action.payload;
        case 'RESET_DATE':
            const dateNow = moment().valueOf();
            return dateNow;
        default: return state;
    }
}

export default dateReducer;